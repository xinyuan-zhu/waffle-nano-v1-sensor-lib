def temp():
    from machine import Pin, ADC   
    adc = ADC(Pin(13))              ##从GPIO13接口处读取数据
    adc.equ(ADC.EQU_MODEL_8)        ##平均采样8次
    a = "%.3g" % ((adc.read() * 0.488 - 32) / 1.8 + 6)      ##计算公式，最后的+6是因为测量的温度稳定比原温度低6度
    return a