# DS1302时钟传感器

## 案例展示

​		一个时钟传感器，可以设置时间，可以对年月日周时分秒进行计时，支持掉电保护（即在没有外部电源时也不会丢失时间数据），功能强大。本案例主要涉及的是其设置时间以及返回现在时间的功能。

下图为烧录后Waffle Nano显示屏上显示的数据。（最上面一行小字是实时的时间（初始化已预先做好，在使用时不要忘记初始化）接着是实时的年，月，日，星期几，时，分，秒）

![](image/20210720104637.jpg)

## 物理连接

### 传感器选择

​		传感器选择如下图所示的型号为DS1302的时钟传感器

![](image/20210720104612.jpg)

### 传感器接线

传感器与Waffle Nano之间的接线如下表所示（dat，clk，rst接线方法不唯一，比较自由，推荐用表中的接脚）

| Waffle Nano | 传感器 |
| ----------- | ------ |
| 3V3         | VCC    |
| GND         | GND    |
| G00         | CLK    |
| G02         | DAT    |
| G01         | RST    |

## 传感器库使用

​		可以[在此](https://gitee.com/jy53181321/waffle-nano-v1-sensor-lib/blob/master/DS1302/code/DS1302.py)获取DS1302.py，将此库通过Waffle Nano的上传功能传到Waffle Nano上

​		可以通过如下代码来引用库

```python
import DS1302
```

​		引用完库之后，需要先将其中的class DS1302实例化

```python
ds=DS1302.DS1302(Pin(0),Pin(2),Pin(1))
```

​		接着就可以调用DS1302中的函数了，例如

```python
display.draw_string(10,10,str(ds.DateTime()),size=1,color=st7789.color565(0,0,0),bg=st7789.WHITE)    #调用DateTime函数，需要先初始化显示屏
```

```python
display.draw_string(10,90,str(ds.Weekday()),size=2,color=st7789.color565(0,0,0),bg=st7789.WHITE)	#调用Weekday函数，同样需要先初始化显示屏
```

​		详细的可调用的函数可以查看DS1302.py

## 案例代码复现

​		可以[在此](https://gitee.com/jy53181321/waffle-nano-v1-sensor-lib/blob/master/DS1302/code/main.py)获取main.py代码，将其内容复制到Waffle Maker编辑器上传输给Waffle Maker，效果应该与本文开头的图一样