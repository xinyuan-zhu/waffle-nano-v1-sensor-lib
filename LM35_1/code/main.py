from machine import SPI,Pin
import st7789,temperature
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))      #初始化spi端口
display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))    
display.init()              #初始化屏幕      
display.fill(st7789.color565(255, 255, 255))      #先清屏
te=temperature.temp()       #调用temperature模块中的temp函数
display.draw_string(10, 100,"Temperature:",size=3,color=st7789.color565(0, 0, 0),bg=st7789.WHITE)
display.draw_string(70, 150,str("%.1f"%float(te)),size=4,color=st7789.color565(0,0,0),bg=st7789.WHITE)
display.draw_string(155, 150,"C",size=4,color=st7789.color565(0,0,0),bg=st7789.WHITE)           #显示数据