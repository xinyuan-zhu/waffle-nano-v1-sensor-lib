from machine import I2C,Pin
import utime
import HDC1080

i2c = I2C(1,sda=Pin(0),scl=Pin(1))
hdc1080 = HDC1080.HDC1080(i2c)

while True:
        print("-------------")
        print ("Temperature = %3.1f C" % hdc1080.readTemperature())
        print ("Humidity = %3.1f %%" % hdc1080.readHumidity())
        print("-------------")

        utime.sleep(1.0)
