# 爱心：

###初始化
from machine import Pin ,SPI ,ADC      # 引入相关库，调用相关函数
import st7789
import math

class   aixin:
    def __init__(self,):
        spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6),mosi=Pin(8))  # //st7787显示屏初始化，SPI设置，st7787设置，填充颜色，加入字符串"heart test"
        display = st7789.ST7789(spi, 240, 240, reset=Pin(11, func=Pin.GPIO, dir=Pin.OUT),dc=Pin(7, func=Pin.GPIO, dir=Pin.OUT))  ###
        display.init()
        display.fill(st7789.WHITE)

    def huaaixin(self,px,py,r,color):
        self.r = r
        self.px = px
        self.py = py
        self.color = color 
        display.fill_circle(int(self.px - self.r * math.cos(45 / 180 * math.pi)), int(self.py - 3 * self.r * math.sin(45 / 180 * math.pi)), self.r,
                    self.color)
        display.fill_circle(int(self.px + self.r * math.cos(45 / 180 * math.pi)), int(self.py - 3 * self.r * math.sin(45 / 180 * math.pi)), self.r,
                    self.color)
        h = self.py
        h_last = int(self.py - 2 * self.r * math.sin(45 / 180 * math.pi))
        dx = 0
        x = 0
        while h > h_last:
            for x in range(self.px - dx, self.px + dx):
                display.pixel(x, h,self.color)
            dx += 1
            h -= 1
ax =aixin()

