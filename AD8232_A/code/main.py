```
###初始化
from machine import Pin ,SPI ,ADC       # 引入相关库，调用相关函数
import utime
import st7789 
import urandom
import math

pin1 = Pin(14, Pin.IN)      #//将引脚设置为信号输入   ###大小写 PIN,变量名
pin2 = Pin(2, Pin.IN)
adc = ADC(Pin(12))              # adc初始化引脚设置，采样次数设置，设置默认电压
adc.equ(ADC.EQU_MODEL_8)

spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))              # //st7787显示屏初始化，SPI设置，st7787设置，填充颜色，加入字符串"heart test"
display = st7789.ST7789(spi, 240, 240, reset=Pin(11, func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7, func=Pin.GPIO, dir=Pin.OUT))###
# display.init()            #屏幕显示黑胡桃问题
display.fill(st7789.WHITE) 

class   aixin:              #爱心函数
    def __init__(self,):
        spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6),mosi=Pin(8))  # //st7787显示屏初始化，SPI设置，st7787设置，填充颜色，加入字符串"heart test"
        display = st7789.ST7789(spi, 240, 240, reset=Pin(11, func=Pin.GPIO, dir=Pin.OUT),dc=Pin(7, func=Pin.GPIO, dir=Pin.OUT))  ###
        display.init()
        display.fill(st7789.WHITE)

    def huaaixin(self,px,py,r,color):
        self.r = r
        self.px = px
        self.py = py
        self.color = color 
        display.fill_circle(int(self.px - self.r * math.cos(45 / 180 * math.pi)), int(self.py - 3 * self.r * math.sin(45 / 180 * math.pi)), self.r,
                    self.color)
        display.fill_circle(int(self.px + self.r * math.cos(45 / 180 * math.pi)), int(self.py - 3 * self.r * math.sin(45 / 180 * math.pi)), self.r,
                    self.color)
        h = self.py
        h_last = int(self.py - 2 * self.r * math.sin(45 / 180 * math.pi))
        dx = 0
        x = 0
        while h > h_last:
            for x in range(self.px - dx, self.px + dx):
                display.pixel(x, h,self.color)
            dx += 1
            h -= 1
ax =aixin()


###开机动画
for r in range(1, 10):
    ax.huaaixin(120,130,r,st7789.RED)
    display.draw_string(0, 0, "Ni Hao", color=st7789.RED, bg=st7789.WHITE, size=5, vertical=True)
    display.draw_string(200, 0, "Hello", color=st7789.RED, bg=st7789.WHITE, size=6, vertical=True)
    utime.sleep_ms(100)
display.fill(st7789.WHITE)      ###
ax.huaaixin(120,150,20,st7789.RED)
display.draw_string(20, 0, "Welcome to", color=st7789.RED, bg=st7789.WHITE, size=4)
display.draw_string(60, 200, "E C G", color=st7789.RED, bg=st7789.WHITE, size=5)
utime.sleep(1)


###测试过程
random = 0           ##初始赋值
heartbeatvalue = 0
heartbeatvalue_str= 0
heart_num = 0
heart_sum = 0
screen_x = 0
screen_y_new =120
screen_y_old = 120
screen_refresh= 0   

display.fill(st7789.WHITE)
display.draw_string(70,0, "Heart test",color=st7789.RED,bg=st7789.WHITE,size=3)  ##显示界面     ###
display.hline(0,120,240,st7789.GREEN)
display.hline(0,121,240,st7789.GREEN)
display.hline(0,119,240,st7789.GREEN)
ax.huaaixin(35,150,20,st7789.RED)

while screen_refresh < 3:  ##屏幕具体显示及更新加载
    if pin1.value() == 1 or pin2.value() == 1 : #对引脚和adc引脚的信号提取
        print("!")
        heartbeatvalue = 1500
        linecolor = st7789.BLACK     #电极片脱落
    else:
        heartbeatvalue = adc.read()
        print(heartbeatvalue)
        linecolor = st7789.RED   #用折线颜色表示电极片是否脱落

    screen_y_new= int(heartbeatvalue / 1500* 240-30)  # 画折线表示波动
    display.line(screen_x,screen_y_old, screen_x+5, screen_y_new, linecolor)
    screen_y_old = screen_y_new
    heartbeatvalue = (2400-heartbeatvalue)/18  #心跳具体值模拟计算

    if heartbeatvalue >= 100: #55->055,100->100                  ###
        heartbeatvalue_str = str(int(heartbeatvalue))
    else :
        heartbeatvalue_str = "0"+str(int(heartbeatvalue))  #空号间隔，int类型转化为str类型，显示心跳值      ###
   
    display.draw_string(10,200,heartbeatvalue_str,color=linecolor,bg=st7789.WHITE,size=5) #显示心跳模拟值
    if screen_refresh == 2 :
        heart_num += 1
        heart_sum += heartbeatvalue

    if screen_x >= 240:    ##屏幕过界更新以及点的x坐标更新
        screen_x = 1
        screen_refresh += 1
        if  screen_refresh == 3 :    #只刷新三次界面
            break
        random = urandom.randint(1,6)      #随机换屏幕颜色
        if random%6 == 0 or random%6 ==2:                 
            display.fill(st7789.YELLOW)             #用st小写，不用ST           ###
        elif random%6 == 1 or random%6 ==4 :
            display.fill(st7789.CYAN)      
        else :
            display.fill(st7789.WHITE)

        display.draw_string(70,0, "Heart test",color=st7789.RED,bg=st7789.WHITE,size=3)      #显示其他位置配置
        display.hline(0,120,240,st7789.GREEN)
        display.hline(0,121,240,st7789.GREEN)
        display.hline(0,119,240,st7789.GREEN)
        ax.huaaixin(35,150,20,st7789.RED)
    else:
        screen_x += 5  
    utime.sleep_ms(100)
display.fill(st7789.BLACK)


###结尾动画
# heartbeatvalue = 100              #末尾界面测试
heartbeatvalue =int(heart_sum/ heart_num)         ##最终结束画面   #求前面心跳的平均值
heartbeatvalue_str= str(int(heartbeatvalue))          ###空号间隔，int类型转化为str类型，显示心跳值
if heartbeatvalue >= 60 and heartbeatvalue <100 :
    heartbeatvalue_str ="0"+str(int(heartbeatvalue))          ###空号间隔，int类型转化为str类型，显示心跳值
    display.fill(st7789.WHITE)
    display.draw_string(80,20,heartbeatvalue_str,color=st7789.GREEN,bg=st7789.WHITE,size=5) 
    display.draw_string(40,200,"Healthy",color=st7789.GREEN,bg=st7789.WHITE,size=5) 
    ax.huaaixin(120,160,30,st7789.GREEN)
elif heartbeatvalue < 60 :
    heartbeatvalue_str ="0"+str(int (heartbeatvalue))
    display.fill(st7789.CYAN) 
    display.draw_string(80,20,heartbeatvalue_str,color=st7789.YELLOW,bg=st7789.WHITE,size=5) 
    display.draw_string(60,200,"Lower",color=st7789.YELLOW,bg=st7789.WHITE,size=5) 
    ax.huaaixin(120,160,30,st7789.YELLOW)
else :
    display.fill(st7789.CYAN)
    display.draw_string(80,20,heartbeatvalue_str,color=st7789.RED,bg=st7789.WHITE,size=5) 
    display.draw_string(40,200,"Quicker",color=st7789.RED,bg=st7789.WHITE,size=5)     
    ax.huaaixin(120,160,30,st7789.RED)
```

