from machine import Pin, I2C
from paj7620 import PAJ7620
import utime

i2c = I2C(1, scl=Pin(1), sda=Pin(0), freq=400000)
print(i2c.scan())  #115
sensor7620 = PAJ7620(i2c,addr = 115)

sensor7620.set_gesture(1,0x0C)

while True:
    print(sensor7620.get_gesture(1))
    utime.sleep(1)