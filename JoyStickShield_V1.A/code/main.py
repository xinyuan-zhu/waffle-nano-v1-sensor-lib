# Never Gonna Give You Up

from machine import ADC, SPI, Pin
from joystickShield import Cmd
import st7789
import urandom
import utime
import math

command = Cmd()

spi = SPI(0, baudrate=40000000, polarity=1, phase=0,bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
screen = st7789.ST7789(spi, 240, 240, reset=Pin(11, func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7, func=Pin.GPIO, dir=Pin.OUT))
screen.init()
screen.draw_string(30, 220, "Edited by Galaxy", size=2)
utime.sleep_ms(1000)

screen.fill(st7789.color565(93, 132, 188))

screen.fill_rect(26, 24, 201, 121, st7789.color565(0,0,0))
screen.fill_rect(24, 22, 201, 121, st7789.color565(93, 132, 188))
screen.rect(24, 22, 201, 121, st7789.color565(254, 145, 18))
screen.vline(74, 22, 120, st7789.color565(254, 145, 18))
screen.vline(124, 22, 120, st7789.color565(254, 145, 18))
screen.vline(174, 22, 120, st7789.color565(254, 145, 18))
screen.hline(24, 52, 200, st7789.color565(254, 145, 18))
screen.hline(24, 82, 200, st7789.color565(254, 145, 18))
screen.hline(24, 112, 200, st7789.color565(254, 145, 18))
# Chart


def generateSeed():
    x=command.exactX()
    y=command.exactY()
    return int(y*math.sin(x)+x*math.cos(y))

# Generate a seed.

score = 0
board = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
flag2048 = False
text=" "
text2=" "


def changeOp():
    x=command.statusX()
    y=command.statusY()
    #up,down,left,right
    if y==1 and x==0:
        return 1
    elif y==-1 and x==0:
        return 2
    elif x==-1 and y==0:
        return 3
    elif x==1 and y==0:
        return 4
    else:
        return 0


def generate():
    position = urandom.randint(0, 15)
    number = int(urandom.randint(2, 5)/2)*2
    if board[int(position/4)][position % 4] != 0:
        return 0
    board[int(position/4)][position % 4] = number
    screen.fill_rect((position % 4)*50+25, int(position/4)*30+23, 49, 29, st7789.color565(177, 167, 0))
    return 1


def start():
    global score
    global flag2048
    score = 0
    flag2048 = False
    board = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    generate()
    while 1:
        if generate() == 1:
            break


def printBoard():
    for i in range(4):
        for j in range(4):
            if 0 <= j <= 2:
                print(board[i][j], end=" ")
            else:
                print(board[i][j])



def judge2048():
    global flag2048
    for i in range(4):
        for j in range(4):
            if board[i][j] == 2048 and flag2048 == False:
                flag2048 = True
                return 1
    return 0


def up():
    global score
    for j in range(0, 4, 1):
        for k in range(0, 3, 1):
            for i in range(k, 3, 1):
                if board[i][j] == 0:
                    board[i][j] = board[i+1][j]
                    board[i+1][j] = 0
        for i in range(0, 3, 1):
            if board[i][j] == board[i+1][j]:
                board[i][j] *= 2
                board[i+1][j] = 0
                score += board[i][j]
        for k in range(0, 3, 1):
            for i in range(k, 3, 1):
                if board[i][j] == 0:
                    board[i][j] = board[i+1][j]
                    board[i+1][j] = 0


def down():
    global score
    for j in range(0, 4, 1):
        for k in range(3, 0, -1):
            for i in range(k, 0, -1):
                if board[i][j] == 0:
                    board[i][j] = board[i-1][j]
                    board[i-1][j] = 0
        for i in range(3, 0, -1):
            if board[i][j] == board[i-1][j]:
                board[i][j] *= 2
                board[i-1][j] = 0
                score += board[i][j]
        for k in range(3, 0, -1):
            for i in range(k, 0, -1):
                if board[i][j] == 0:
                    board[i][j] = board[i-1][j]
                    board[i-1][j] = 0


def left():
    global score
    for i in range(0, 4, 1):
        for k in range(0, 3, 1):
            for j in range(k, 3, 1):
                if board[i][j] == 0:
                    board[i][j] = board[i][j+1]
                    board[i][j+1] = 0
        for j in range(0, 3, 1):
            if board[i][j] == board[i][j+1]:
                board[i][j] *= 2
                board[i][j+1] = 0
                score += board[i][j]
        for k in range(0, 3, 1):
            for j in range(k, 3, 1):
                if board[i][j] == 0:
                    board[i][j] = board[i][j+1]
                    board[i][j+1] = 0


def right():
    global score
    for i in range(0, 4, 1):
        for k in range(3, 0, -1):
            for j in range(k, 0, -1):
                if board[i][j] == 0:
                    board[i][j] = board[i][j-1]
                    board[i][j-1] = 0
        for j in range(3, 0, -1):
            if board[i][j] == board[i][j-1]:
                board[i][j] *= 2
                board[i][j-1] = 0
                score += board[i][j]
        for k in range(3, 0, -1):
            for j in range(k, 0, -1):
                if board[i][j] == 0:
                    board[i][j] = board[i][j-1]
                    board[i][j-1] = 0


def sameNeighbourHorizontal():
    for i in range(4):
        for j in range(3):
            if board[i][j] == board[i][j+1] and board[i][j] != 0:
                return 1
    return 0


def sameNeighbourVertical():
    for j in range(4):
        for i in range(3):
            if board[i][j] == board[i+1][j] and board[i][j] != 0:
                return 1
    return 0


def gameOver():
    for i in range(16):
        if board[int(i/4)][i % 4] == 0:
            return 0
    if sameNeighbourHorizontal()==1 or sameNeighbourVertical()==1:
        return 0
    return 1


def movableLeft():
    if sameNeighbourHorizontal() == 1:
        return 1
    for i in range(4):
        # status: -1-have not find 0s, 0-have 0s on the left, 1-have 0s on the left and other numbers on the right
        status = -1
        for j in range(4):
            if status == -1 and board[i][j] == 0:
                status = 0
            elif status == 0 and board[i][j] != 0:
                status = 1
                break
        if status == 1:
            return 1
    return 0


def movableRight():
    if sameNeighbourHorizontal() == 1:
        return 1
    for i in range(4):
        # status: -1-have not find 0s, 0-have 0s on the right, 1-have 0s on the right and other numbers on the left
        status = -1
        for j in range(3, -1, -1):
            if status == -1 and board[i][j] == 0:
                status = 0
            elif status == 0 and board[i][j] != 0:
                status = 1
                break
        if status == 1:
            return 1
    return 0


def movableUp():
    if sameNeighbourVertical() == 1:
        return 1
    for j in range(4):
        # status: -1-have not find 0s, 0-have 0s above, 1-have 0s above and other numbers below
        status = -1
        for i in range(4):
            if status == -1 and board[i][j] == 0:
                status = 0
            elif status == 0 and board[i][j] != 0:
                status = 1
                break
        if status == 1:
            return 1
    return 0


def movableDown():
    if sameNeighbourVertical() == 1:
        return 1
    for j in range(4):
        # status: -1-have not find 0s, 0-have 0s below, 1-have 0s below and other numbers above
        status = -1
        for i in range(3, -1, -1):
            if status == -1 and board[i][j] == 0:
                status = 0
            elif status == 0 and board[i][j] != 0:
                status = 1
                break
        if status == 1:
            return 1
    return 0


def printNumber():
    for i in range(4):
        for j in range(4):
            if board[i][j] != 0:
                screen.draw_string(j*50+32, i*30+32, str(board[i][j]), color=st7789.BLACK, bg=st7789.BLACK, size=2)
                screen.draw_string(j*50+30, i*30+30, str(board[i][j]), size=2)


def printInfo():
    global score
    global text
    screen.fill_rect(10, 150, 230, 90, st7789.color565(93, 132, 188))
    screen.draw_string(12, 152, "Score", color=st7789.BLACK, bg=st7789.BLACK, size=3)
    screen.draw_string(10, 150, "Score", size=3)
    screen.draw_string(122, 152, str(score), color=st7789.BLACK, bg=st7789.BLACK, size=3)
    screen.draw_string(120, 150, str(score), size=3)
    screen.draw_string(12, 182, text, color=st7789.BLACK, bg=st7789.BLACK, size=3)
    screen.draw_string(10, 180, text, size=3)
    screen.draw_string(12, 212, text2, color=st7789.BLACK, bg=st7789.BLACK, size=3)
    screen.draw_string(10, 210, text2, size=3)


def restoreNewNumber():
    global text
    screen.fill_rect(10, 10, 230, 139, st7789.color565(93, 132, 188))
    screen.fill_rect(26, 24, 201, 121, st7789.color565(0,0,0))
    screen.fill_rect(24, 22, 201, 121, st7789.color565(93, 132, 188))
    screen.rect(24, 22, 201, 121, st7789.color565(254, 145, 18))
    screen.vline(74, 22, 120, st7789.color565(254, 145, 18))
    screen.vline(124, 22, 120, st7789.color565(254, 145, 18))
    screen.vline(174, 22, 120, st7789.color565(254, 145, 18))
    screen.hline(24, 52, 200, st7789.color565(254, 145, 18))
    screen.hline(24, 82, 200, st7789.color565(254, 145, 18))
    screen.hline(24, 112, 200, st7789.color565(254, 145, 18))
    for i in range(4):
        for j in range(4):
            if board[i][j] != 0:
                screen.fill_rect(j*50+25, i*30+23, 49, 29,st7789.color565(45, 78, 123))

text2="B-start"
printInfo()
while 1:
    if command.statusB()==0:
        break
text2=" "
printInfo()
while 1:
    score = 0
    board = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    flag2048 = False
    restoreNewNumber()
    text="Put joystick"
    text2="at a"
    printInfo()
    utime.sleep_ms(1000)
    text="random place"
    text2=" "
    printInfo()
    utime.sleep_ms(1000)
    text="to start in 3"
    printInfo()
    utime.sleep_ms(1000)
    text="to start in 2"
    printInfo()
    utime.sleep_ms(1000)
    text="to start in 1"
    printInfo()
    utime.sleep_ms(1000)
    urandom.seed(generateSeed())
    start()
    score = 0
    while 1:
        available = 0
        printNumber()
        if gameOver() == 1:
            text = "Game over"
            printInfo()
            utime.sleep_ms(1000)
            text="Thanks for"
            text2="playing"
            printInfo()
            utime.sleep_ms(1000)
            text="2048 by Galaxy"
            text2= "C-restart"
            printInfo()
            while 1:
                if command.statusC()==0:
                    break
            break
        text = "Input option"
        printInfo()
        while 1:
            op=changeOp()
            if op!=0:
                break
        if op == 1:
            available = movableUp()
            if available == 0:
                text = "Illegal option"
                printInfo()
                utime.sleep_ms(500)
                continue
            else:
                text = " "
                printInfo()
            up()
        elif op == 2:
            available = movableDown()
            if available == 0:
                text = "Illegal option"
                printInfo()
                utime.sleep_ms(500)
                continue
            else:
                text = " "
                printInfo()
            down()
        elif op == 3:
            available = movableLeft()
            if available == 0:
                text = "Illegal option"
                printInfo()
                utime.sleep_ms(500)
                continue
            else:
                text = " "
                printInfo()
            left()
        elif op == 4:
            available = movableRight()
            if available == 0:
                text = "Illegal option"
                printInfo()
                utime.sleep_ms(500)
                continue
            else:
                text = " "
                printInfo()
            right()
        else:
            text = "Illegal option"
            printInfo()
            utime.sleep_ms(500)
            continue
        restoreNewNumber()
        text="Release stick"
        printInfo()
        while 1:
            if command.statusX()==0 and command.statusY()==0:
                break
        while 1:
            if generate() == 1:
                break
        if judge2048() == 1:
            text = "Good job!"
            printInfo()
            utime.sleep_ms(1000)

# Say Goodbye
