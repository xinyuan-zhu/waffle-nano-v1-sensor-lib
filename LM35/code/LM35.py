def temp():
    from machine import Pin, ADC#导入 machine 模块的硬件类 Pin和数模转换类ADC
    adc = ADC(Pin(13))#把waffle nano默认的GPIO 13号引脚设置为ADC通道3
    adc.equ(ADC.EQU_MODEL_8)#设置ADC的平均计算的采样次数为8次
    adc.atten(ADC.CUR_BAIS_DEFAULT)#将电源电压设为默认值
    a = "%.3g" % ((adc.read() * 0.488 - 32) / 1.8-5)#读取采样值并计算，实操后发现测得温度偏高5℃，所以减去5℃
    return a#返回温度值