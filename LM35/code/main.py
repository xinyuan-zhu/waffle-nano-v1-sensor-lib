from machine import SPI, Pin#导入通信以及引脚相关的库
import st7789,LM35,utime#导入屏幕库驱动，温度模块，时间模块
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))##构造spi对象
display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))#构造屏幕控制对象
display.init()#屏幕初始化
display.fill(st7789.color565(255, 255, 255))#设置屏幕背景为白色
while(1):#不断循环获取实时温度
    te=LM35.temp()#获取温度
    display.draw_string(70, 150, str(te)+" C",size=3,color=st7789.color565(0, 0, 0))#在屏幕上显示温度
    utime.sleep(1)#刷新温度的时间间隔
    display.fill(st7789.color565(255, 255, 255))#设置屏幕背景为白色