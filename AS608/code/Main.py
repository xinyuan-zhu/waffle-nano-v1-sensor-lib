from machine import UART, Pin, SPI
import st7789
import  utime, network
from AS608 import uart_init, fingerprint1, buffer1, fingerprint2, buffer2, compare, search, clear
from MQTT import MQTTClient

#MQTT交互程序设计
global Mymsg
mymsg=None

wl = network.WLAN(network.STA_IF)
wl.active(1)
wl.connect('LAPTOP-8FIJU2IS 3092',';F23595m',security=network.AUTH_PSK)

client_id = "xcy"
mqtt_server = "thingworx.zucc.edu.cn"
port=1883
topic_sub = "xucongyu"
topic_pub = "xucongyu"
mqtt_user = ""
mqtt_password = ""

def sub_cb(topic, msg):
    global mymsg
    mymsg=msg
    #print((topic, msg))
    #client.publish(topic_pub, msg)

def connect_and_subscribe():
    global client_id, mqtt_server, topic_sub, mqtt_user, mqtt_password
    client = MQTTClient(client_id, mqtt_server, user=mqtt_user, password=mqtt_password, port=port)
    client.set_callback(sub_cb)
    client.connect()
    client.subscribe(topic_sub)
    print('Connected to %s MQTT broker, subscribed to %s topic' % (mqtt_server, topic_sub))
    return client

def restart_and_reconnect():
    print('Failed to connect to MQTT broker. Reconnecting...')
    utime.sleep(10)
    machine.reset()

try:
    client = connect_and_subscribe()
except OSError as e:
    restart_and_reconnect()


uart = uart_init()#uart初始化
str1 = b'\xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x06\x06\x02\x00\x01\x00\x10'  # 对比储存的一个案例字符串，这里我们默认从1开始

spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
display = st7789.ST7789(spi, 240, 240, reset=Pin(11, func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7, func=Pin.GPIO, dir=Pin.OUT))
display.init()

flag=0#构建退出程序的标志

while True:

    display.fill(st7789.color565(255, 255, 255))
    x = 0
    y = 0
    while True:  # 绘制网格界面
        display.vline(x, 0, 240, st7789.color565(255, 0, 0))
        display.hline(0, y, 240, st7789.color565(255, 0, 0))
        x = x + 30
        y = y + 30
        if (x >= 240):
            break

    utime.sleep(0.5)
    display.draw_string(10, 10, "Welcome", size=4, color=st7789.color565(255, 0, 0))  # 界面初始化
    utime.sleep(0.5)
    display.draw_string(10, 50, "to", size=4, color=st7789.color565(0, 0, 255))  # 界面初始化
    utime.sleep(0.5)
    display.draw_string(10, 90, "Login", size=4, color=st7789.color565(255, 97, 0))  # 界面初始化
    utime.sleep(0.5)
    display.draw_string(10, 130, "Interface", size=4, color=st7789.color565(0, 255, 0))  # 界面初始化

    while True:
        client.subscribe(topic_sub)
        if mymsg!=None:
            Mymsg=str(mymsg,'utf-8')
            if Mymsg=='page 1':#菜单第一页
                client.subscribe(topic_sub)
                mymsg=None
                display.fill(st7789.color565(0, 255, 0))  # 进行交互界面

                display.draw_string(0, 10, "Function 1:", size=3, color=st7789.color565(65, 105, 225),
                                    bg=st7789.color565(65, 105, 225))  # 界面初始化
                display.draw_string(0, 60, "Input fingerprint", size=2, color=st7789.color565(65, 105, 225),
                                    bg=st7789.color565(65, 105, 225))  # 界面初始化
                display.draw_string(0, 120, "Function 2:", size=3, color=st7789.color565(65, 105, 225),
                                    bg=st7789.color565(65, 105, 225))  # 界面初始化
                display.draw_string(0, 170, "Upload fingerprint", size=2, color=st7789.color565(65, 105, 225),
                                    bg=st7789.color565(65, 105, 225))  # 界面初始化
                utime.sleep(2)
                break

            if Mymsg=='page 2':#菜单第二页
                client.subscribe(topic_sub)
                mymsg=None
                display.fill(st7789.color565(0, 255, 0))  # 进行交互界面

                display.draw_string(0, 10, "Function 3:", size=3, color=st7789.color565(65, 105, 225),
                                    bg=st7789.color565(65, 105, 225))  # 界面初始化
                display.draw_string(0, 60, "Search for fingerprint", size=2, color=st7789.color565(65, 105, 225),
                                    bg=st7789.color565(65, 105, 225))  # 界面初始化
                display.draw_string(0, 120, "Function 4:", size=3, color=st7789.color565(65, 105, 225),
                                    bg=st7789.color565(65, 105, 225))  # 界面初始化
                display.draw_string(0, 170, "Clear the library", size=2, color=st7789.color565(65, 105, 225),
                                    bg=st7789.color565(65, 105, 225))  # 界面初始化
                utime.sleep(2)
                break

            if Mymsg=='0':#记录到0的时候退出程序
                flag=1
                break

            if Mymsg=='1':#开始指纹录入
                client.subscribe(topic_sub)
                mymsg=None
                display.fill(st7789.color565(65, 105, 225))  # 初始化界面，接下来开始录入部分
                utime.sleep(0.3)
                display.draw_string(10, 10, "Start typing fingerprint", size=1, color=st7789.color565(0, 0, 0),
                                    bg=st7789.color565(65, 105, 225))  # 开始录入指纹
                fingerprint1(display, uart)  # 手指放在传感器上，过一段时间进行第一次录入
                buffer1(display, uart)  # 第一次录入存到buffer1缓冲区，显示传感器是否成功录入指纹
                fingerprint2(display, uart)  # 延时一段时间后会进行第二次录入
                buffer2(display, uart)  # 第二次录入存到buffer2缓冲区，显示传感器是否成功录入指纹
                break

            elif Mymsg=='2':#开始指纹传送功能
                client.subscribe(topic_sub)
                mymsg=None
                display.fill(st7789.color565(160, 32, 240))  # 录入部分结束，再次初始化界面，接下来开始储存部分
                utime.sleep(0.3)
                compare(display, uart, str1)  # 进行两次指纹录入的对比，若精确度高，则合成模板存入buffer1和buffer2中，延时一段时间后就会自动把模板传入指纹数据库的相应位置
                str1 = bytearray(str1)
                i = int(str1[12])
                i += 1
                j = int(str1[14])
                j += 1
                str1[12] = i
                str1[14] = j  # 相当于从ID=1开始，每一次循环后都会使ID+1，每一次都往后延顺一个用户，这样就可以实现与用户的简单交互
                break

            elif Mymsg=='3':#开始指纹搜索功能
                client.subscribe(topic_sub)
                mymsg=None
                display.fill(st7789.color565(255, 255, 0))  # 录入及储存部分结束，再次初始化界面，接下来开始搜索部分
                search(display, uart)
                break

            elif Mymsg=='4':#执行清空库功能
                client.subscribe(topic_sub)
                mymsg=None
                display.fill(st7789.color565(255, 97, 0))  # 录入及储存部分结束，再次初始化界面，接下来开始搜索部分
                clear(display, uart)
                break

    if flag==1:
        break
    display.fill(st7789.color565(0, 200, 0))  # 填充绿色进入休息时间
    display.draw_string(60, 60, 'Thanks', size=4, color=st7789.color565(255, 255, 255))
    display.draw_string(0, 100, 'Please go to', size=4, color=st7789.color565(255, 255, 255))
    display.draw_string(0, 150, 'the next one', size=4, color=st7789.color565(255, 255, 255))  # 每一个用户实现功能之后，会有一个短暂的休息
    utime.sleep(4)#继续循环执行功能


display.fill(st7789.color565(0, 255, 0))#开始结束界面的构建


x = 0
y = 0
while True:# 绘制网格界面
    display.vline(x, 0, 240, st7789.color565(0, 0, 0))
    display.hline(0, y, 240, st7789.color565(0, 0, 0))
    x = x + 30
    y = y + 30
    if (x >= 240):
        break

display.draw_string(30, 60, 'Thank you', size=4, color=st7789.color565(61, 89, 171), bg=st7789.color565(61, 89, 171))
display.draw_string(10, 120, 'patronizing', size=4, color=st7789.color565(30, 144, 255),bg=st7789.color565(30, 144, 255))  # 结束